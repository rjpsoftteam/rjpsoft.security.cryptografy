﻿using System;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;

namespace RJPSoft.Security.Cryptography
{
    /// <summary>
    /// Handles Cryptografy
    /// </summary>
    public class CryptoHandler
    {
        #region Public Methods

        #region RSA

        /// <summary>
        /// Decrypts using RSA.
        /// </summary>
        /// <param name="data">The data to decrypt</param>
        /// <param name="privateKey">The decryption private key</param>
        /// <returns>Thge decrypted data</returns>
        public static string DecryptRSA(string data, string privateKey)
        {
            var keyBuffer = CryptographicBuffer.DecodeFromBase64String(privateKey);
            var dataBytes = Convert.FromBase64String(data);

            //Tipo de chave tem que ser esse para ser intercambiavel com outros algoritmos
            var asym = AsymmetricKeyAlgorithmProvider.OpenAlgorithm(AsymmetricAlgorithmNames.RsaPkcs1);
            var criptoKey = asym.ImportKeyPair(keyBuffer, CryptographicPrivateKeyBlobType.Capi1PrivateKey);

            var plainBuffer = CryptographicEngine.Decrypt(criptoKey, dataBytes.AsBuffer(), null);
            CryptographicBuffer.CopyToByteArray(plainBuffer, out var plainBytes);

            return Encoding.UTF8.GetString(plainBytes, 0, plainBytes.Length);
        }

        /// <summary>
        /// Encrypts using RSA.
        /// </summary>
        /// <param name="data">The data to be encrypted</param>
        /// <param name="publicKey">The encryption public key.</param>
        /// <returns>The encrypted data</returns>
        public static string EncryptRSA(string data, string publicKey)
        {
            var keyBuffer = CryptographicBuffer.DecodeFromBase64String(publicKey);

            //Tipo de chave tem que ser esse para ser intercambiavel com outros algoritmos
            var asym = AsymmetricKeyAlgorithmProvider.OpenAlgorithm(AsymmetricAlgorithmNames.RsaPkcs1);
            var criptoKey = asym.ImportPublicKey(keyBuffer, CryptographicPublicKeyBlobType.Capi1PublicKey);

            var plainBuffer = CryptographicBuffer.ConvertStringToBinary(data, BinaryStringEncoding.Utf8);
            var encryptedBuffer = CryptographicEngine.Encrypt(criptoKey, plainBuffer, null);

            CryptographicBuffer.CopyToByteArray(encryptedBuffer, out var encryptedBytes);

            return Convert.ToBase64String(encryptedBytes);
        }

        /// <summary>
        /// Create private and public keys pair.
        /// </summary>
        /// <param name="keySize">The size of the key</param>
        /// <returns>The private and public keys</returns>
        public static Tuple<string, string> CreateRSAKeyPair(int keySize)
        {
            var asym = AsymmetricKeyAlgorithmProvider.OpenAlgorithm(AsymmetricAlgorithmNames.RsaPkcs1);
            var key = asym.CreateKeyPair((uint) keySize);

            //Tipo de chave tem que ser esse para ser intercambiavel com outros algoritmos
            var privateKeyBuffer = key.Export(CryptographicPrivateKeyBlobType.Capi1PrivateKey);
            var publicKeyBuffer = key.ExportPublicKey(CryptographicPublicKeyBlobType.Capi1PublicKey);

            CryptographicBuffer.CopyToByteArray(privateKeyBuffer, out var privateKeyBytes);
            CryptographicBuffer.CopyToByteArray(publicKeyBuffer, out var publicKeyBytes);

            var privateKey = Convert.ToBase64String(privateKeyBytes);
            var publicKey = Convert.ToBase64String(publicKeyBytes);

            return new Tuple<string, string>(privateKey, publicKey);
        }

        #endregion RSA

        #region AES

        /// <summary>
        /// Decrypts using aes.
        /// </summary>
        /// <param name="encryptedData">The data to be encrypted.</param>
        /// <param name="key">The encryption key.</param>
        /// <param name="iv">The initialization vector.</param>
        /// <returns>The decrypted data</returns>
        public static string DecryptAES(string encryptedData, string key, string iv)
        {
            var keyBuffer = CryptographicBuffer.ConvertStringToBinary(key, BinaryStringEncoding.Utf8);
            var ivBuffer = CryptographicBuffer.ConvertStringToBinary(iv, BinaryStringEncoding.Utf16LE);
            var cipherBuffer = CryptographicBuffer.CreateFromByteArray(Convert.FromBase64String(encryptedData));

            // Derive key material for password size 32 bytes for AES256 algorithm
            var keyDerivationProvider = KeyDerivationAlgorithmProvider.OpenAlgorithm(KeyDerivationAlgorithmNames.Pbkdf2Sha1);

            // using salt and 1000 iterations
            var pbkdf2Parms = KeyDerivationParameters.BuildForPbkdf2(ivBuffer, 1000);

            // create a key based on original key and derivation parmaters
            var keyOriginal = keyDerivationProvider.CreateKey(keyBuffer);
            var keyMaterial = CryptographicEngine.DeriveKeyMaterial(keyOriginal, pbkdf2Parms, 32);
            var derivedPwKey = keyDerivationProvider.CreateKey(keyBuffer);

            // derive buffer to be used for encryption salt from derived password key
            var ivMaterial = CryptographicEngine.DeriveKeyMaterial(derivedPwKey, pbkdf2Parms, 16);

            var symProvider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
            // create symmetric key from derived password material
            var symmKey = symProvider.CreateSymmetricKey(keyMaterial);

            // encrypt data buffer using symmetric key and derived salt material
            var resultBuffer = CryptographicEngine.Decrypt(symmKey, cipherBuffer, ivMaterial);
            var result = CryptographicBuffer.ConvertBinaryToString(BinaryStringEncoding.Utf16LE, resultBuffer);
            return result;
        }

        /// <summary>
        /// Encrypts using aes.
        /// </summary>
        /// <param name="data">The data to be encrypted.</param>
        /// <param name="key">The encryption key.</param>
        /// <param name="iv">The initialization vector.</param>
        /// <returns>The encrypted data</returns>
        public static string EncryptAES(string data, string key, string iv)
        {
            var keyBuffer = CryptographicBuffer.ConvertStringToBinary(key, BinaryStringEncoding.Utf8);
            var ivBuffer = CryptographicBuffer.ConvertStringToBinary(iv, BinaryStringEncoding.Utf16LE);
            var plainBuffer = CryptographicBuffer.ConvertStringToBinary(data, BinaryStringEncoding.Utf16LE);

            // Derive key material for password size 32 bytes for AES256 algorithm
            var keyDerivationProvider = KeyDerivationAlgorithmProvider.OpenAlgorithm(KeyDerivationAlgorithmNames.Pbkdf2Sha1);

            // using salt and 1000 iterations
            var pbkdf2Parms = KeyDerivationParameters.BuildForPbkdf2(ivBuffer, 1000);

            // create a key based on original key and derivation parmaters
            var keyOriginal = keyDerivationProvider.CreateKey(keyBuffer);
            var keyMaterial = CryptographicEngine.DeriveKeyMaterial(keyOriginal, pbkdf2Parms, 32);
            var derivedKey = keyDerivationProvider.CreateKey(keyBuffer);

            // derive buffer to be used for encryption salt from derived password key
            var ivMaterial = CryptographicEngine.DeriveKeyMaterial(derivedKey, pbkdf2Parms, 16);

            var symProvider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
            // create symmetric key from derived password key
            var symmKey = symProvider.CreateSymmetricKey(keyMaterial);

            // encrypt data buffer using symmetric key and derived salt material
            var resultBuffer = CryptographicEngine.Encrypt(symmKey, plainBuffer, ivMaterial);
            CryptographicBuffer.CopyToByteArray(resultBuffer, out var result);

            return Convert.ToBase64String(result);
        }

        public static Tuple<string, string> CreateAESKeyIVPair()
        {
            var Algorithm = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
            var keymaterial = CryptographicBuffer.GenerateRandom((256 + 7) / 8);

            var iv = new byte[16];
            new Random().NextBytes(iv);
            return new Tuple<string, string>(Convert.ToBase64String(iv), Convert.ToBase64String(keymaterial.ToArray()));
        }

        #endregion AES

        /// <summary>
        /// Hashes the value using SHA256
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The hashed value</returns>
        public static string ApplySha256(string value)
        {
            var sha256 = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha256);
            var buffer = CryptographicBuffer.ConvertStringToBinary(value, BinaryStringEncoding.Utf8);

            var hashedBytes = sha256.HashData(buffer);
            var cyphedValue = CryptographicBuffer.EncodeToBase64String(hashedBytes);

            return cyphedValue;
        }

        #endregion Public Methods
    }
}