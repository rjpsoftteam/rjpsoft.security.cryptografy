﻿using NUnit.Framework;
using System.Collections;

namespace RJPSoft.Security.Cryptography._35.Tests
{
    [TestFixture]
    public class CryptographyTests : AssertionHelper
    {
        private const string winRtRSAPublicKey = "BgIAAACkAABSU0ExAAQAAAEAAQDJsgfTWi4DmJbqS4psf3aUioVTIbqPjQjF/cP0CN0+1ViITXGKW/inTkWf9aHbi5B49QmYRMmw2psR1UaBwDB1sCzlmcQ/0geCZVD6Y5AwknMgsy9QzJLsaiOcX2eMDc7SOmzRyC5nEtRgZGBXSNDYsn6VLH2VtAuiTaPyB7eDuw==";
        private const string winRtRSAPrivateKey = "BwIAAACkAABSU0EyAAQAAAEAAQDJsgfTWi4DmJbqS4psf3aUioVTIbqPjQjF/cP0CN0+1ViITXGKW/inTkWf9aHbi5B49QmYRMmw2psR1UaBwDB1sCzlmcQ/0geCZVD6Y5AwknMgsy9QzJLsaiOcX2eMDc7SOmzRyC5nEtRgZGBXSNDYsn6VLH2VtAuiTaPyB7eDuw84zrT0RDpn5AmPgd4rEF7pj9hIEF2Hg3l6Ny8vXrxMeNpXPvRJ/ar0lW4ModYXhiOsDqCfZSGkPA6BXpJjr9ynzwP3q3TJEe2GeptILvaWNViy782ZkINrrA2+djvRpd3F4Vftmu3QEEcEihE78OF0W/z3DqKClO37i5WXdIXZ33W9GmK+N6qZB47AU9Ek8FXSoFxQmpgKqA4hfUrBGr3vIOpMDI5XCuFyQPNt4ECdYexzB0JNd2Rcwhhf26Q1bk07xo+IT3l4aEserqwhvNLiAX5FbJaNMxiBExTpSTJpGlp9fEZ/rvfIZVmmDskzaObJylKCVTpsRl/lgiu+STF3Qvxm+3ivMBAFlyacAgIMTWlXlfGhDDHiy23hz+StZrxVPS8B08n2b9eay+3OdX7p1cKXKMdw78LIMvS+b+VcjW65Yh+whmSRnQur65GPGaG3b0WE3TEqUQ/TasFFX9ybOSRupMK5PGIk7GfO0GyEMztMwExS/rpXMjOimLHPY/DfCFiS9UTIN8xu8OUBoaIwEUsxOZZtXsnyIsdJSb64T7GKH/F4FZMzlYxuLpJARCejkqF2pYQDiVVy+BJWMbI=";

        private const string winRtAESIv = "B3Db5tmb5Gc6ELzG9imncw==";
        private const string winRtAESKey = "3kxdUtMyKT2T+Regcx97mWbbxDVVO2tsOH9E6qTmuaU=";

        [Test(Description = "[Net 3.5] Encrypt and decrypt with generated Net35 key should work"),
         TestCaseSource(typeof(MyDataClass), "TestCases")]
        public void EncryptAndDecryptWithGeneratedNet35KeyShouldWork(string value)
        {
            var keys = CryptoHandler.CreateRSAKeyPair(2560);
            var privateKey = keys.Item1;
            var publicKey = keys.Item2;

            var encoded = CryptoHandler.EncryptRSA(value, publicKey);
            var decoded = CryptoHandler.DecryptRSA(encoded, privateKey);

            Expect(encoded, Not.EqualTo(""));
            Expect(encoded, Not.EqualTo(decoded));
            Expect(decoded, EqualTo(value));
        }

        [Test(Description = "[Net 3.5] Encrypt and decrypt with generated WinRT key should work"),
         TestCaseSource(typeof(MyDataClass), "TestCases")]
        public void EncryptAndDecryptWithGeneratedWinRTKeyWork(string value)
        {
            var encoded = CryptoHandler.EncryptRSA(value, winRtRSAPublicKey);
            var decoded = CryptoHandler.DecryptRSA(encoded, winRtRSAPrivateKey);

            Expect(encoded, Not.EqualTo(""));
            Expect(encoded, Not.EqualTo(decoded));
            Expect(decoded, EqualTo(value));

        }

        [Test(Description = "[Net 3.5] Encrypt and decrypt with pre generated AES key should work"),
         TestCaseSource(typeof(MyDataClass), "TestCases")]
        public void TestAES(string value)
        {
            var encoded = CryptoHandler.EncryptAES(value, winRtAESKey, winRtAESIv);
            var decoded = CryptoHandler.DecryptAES(encoded, winRtAESKey, winRtAESIv);

            Expect(encoded, Not.EqualTo(""));
            Expect(encoded, Not.EqualTo(value));
            Expect(encoded, Not.EqualTo(decoded));
            Expect(decoded, EqualTo(value));
        }

        [Test(Description = "[Net 3.5] Encrypt and decrypt with generated AES key should work"),
         TestCaseSource(typeof(MyDataClass), "TestCases")]
        public void TestAESNet35(string value)
        {
            var keyPair = CryptoHandler.CreateAESKeyIVPair();
            var key = keyPair.Item2;
            var iv = keyPair.Item1;
            var encoded = CryptoHandler.EncryptAES(value, key, iv);
            var decoded = CryptoHandler.DecryptAES(encoded, key, iv);

            Expect(encoded, Not.EqualTo(""));
            Expect(encoded, Not.EqualTo(value));
            Expect(encoded, Not.EqualTo(decoded));
            Expect(decoded, EqualTo(value));
        }

        public class MyDataClass
        {
            public static IEnumerable TestCases
            {
                get
                {
                    yield return new TestCaseData("teste de string 1");
                    yield return new TestCaseData("not empty");
                    yield return new TestCaseData("one more time with @/*");
                    yield return new TestCaseData("uma string em pt com josé e ramelê");
                }
            }
        }
    }
}
