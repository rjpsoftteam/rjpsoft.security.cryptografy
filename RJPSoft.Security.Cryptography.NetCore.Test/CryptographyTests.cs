﻿using FluentAssertions;
using NUnit.Framework;
using System.Collections;

namespace RJPSoft.Security.Cryptography._35.Tests
{
    [TestFixture]
    public class CryptographyTests
    {
        private const string winRtRSAPublicKey = "BgIAAACkAABSU0ExAAQAAAEAAQDJsgfTWi4DmJbqS4psf3aUioVTIbqPjQjF/cP0CN0+1ViITXGKW/inTkWf9aHbi5B49QmYRMmw2psR1UaBwDB1sCzlmcQ/0geCZVD6Y5AwknMgsy9QzJLsaiOcX2eMDc7SOmzRyC5nEtRgZGBXSNDYsn6VLH2VtAuiTaPyB7eDuw==";
        private const string winRtRSAPrivateKey = "BwIAAACkAABSU0EyAAQAAAEAAQDJsgfTWi4DmJbqS4psf3aUioVTIbqPjQjF/cP0CN0+1ViITXGKW/inTkWf9aHbi5B49QmYRMmw2psR1UaBwDB1sCzlmcQ/0geCZVD6Y5AwknMgsy9QzJLsaiOcX2eMDc7SOmzRyC5nEtRgZGBXSNDYsn6VLH2VtAuiTaPyB7eDuw84zrT0RDpn5AmPgd4rEF7pj9hIEF2Hg3l6Ny8vXrxMeNpXPvRJ/ar0lW4ModYXhiOsDqCfZSGkPA6BXpJjr9ynzwP3q3TJEe2GeptILvaWNViy782ZkINrrA2+djvRpd3F4Vftmu3QEEcEihE78OF0W/z3DqKClO37i5WXdIXZ33W9GmK+N6qZB47AU9Ek8FXSoFxQmpgKqA4hfUrBGr3vIOpMDI5XCuFyQPNt4ECdYexzB0JNd2Rcwhhf26Q1bk07xo+IT3l4aEserqwhvNLiAX5FbJaNMxiBExTpSTJpGlp9fEZ/rvfIZVmmDskzaObJylKCVTpsRl/lgiu+STF3Qvxm+3ivMBAFlyacAgIMTWlXlfGhDDHiy23hz+StZrxVPS8B08n2b9eay+3OdX7p1cKXKMdw78LIMvS+b+VcjW65Yh+whmSRnQur65GPGaG3b0WE3TEqUQ/TasFFX9ybOSRupMK5PGIk7GfO0GyEMztMwExS/rpXMjOimLHPY/DfCFiS9UTIN8xu8OUBoaIwEUsxOZZtXsnyIsdJSb64T7GKH/F4FZMzlYxuLpJARCejkqF2pYQDiVVy+BJWMbI=";

        private const string winRtAESIv = "B3Db5tmb5Gc6ELzG9imncw==";
        private const string winRtAESKey = "3kxdUtMyKT2T+Regcx97mWbbxDVVO2tsOH9E6qTmuaU=";

        private const string net35RSAPublicKey = "BgIAAACkAABSU0ExAAQAAAEAAQChUw2t78vUll1WA2Qaak0pCncNPOOPaHbVK3TtrcI7zKfVOs7OvBIvz02YSEkxuyGkI1Hl7HGw0pHzJwyO1XS1Ro45l5BO7cYWpyoobzs/qfaClg/e0rk5j1xEZL2aPVvU2ydvMKnFU7ZWuqVSZAzm50dYFU9L/h7vc4nDYRW0zA==";
        private const string net35RSAPrivateKey = "BwIAAACkAABSU0EyAAQAAAEAAQChUw2t78vUll1WA2Qaak0pCncNPOOPaHbVK3TtrcI7zKfVOs7OvBIvz02YSEkxuyGkI1Hl7HGw0pHzJwyO1XS1Ro45l5BO7cYWpyoobzs/qfaClg/e0rk5j1xEZL2aPVvU2ydvMKnFU7ZWuqVSZAzm50dYFU9L/h7vc4nDYRW0zJtXdGWM2M1UIoFY2AXGrXkMv63eb8oMOHKpw4y0x9rxUAptxX7OfPqFJK0ve4pxtQcELHbFD3JQsdcptWNquN9z+3/WdhIkJ+6Sfnu+ifm6cWoIORgASSZ+iPIuaZxj6Wmmb2edd8Cuw23zrsatHK4rEmBGxSZvlLPA9QzePD3q38SlLc5jYP/7YokNBgI9MH6bz4xsv2ZqcqmVu/x+EAqZwiBPKLUJbrYFXajYcSiO8SwAsVL0/SbKUbrUyau7Hr24vJ1L6wPMYUP38pkDGYsRuxv/2xB6B/K4YDhAzEkljKSVpgD4UT2wW2N5u5ps5qL5qclEwhDA7CMmpF04r+gEDJamwS08xNaVI6zjiImzb157BvpotYMFJx5Sh5Yak5M2D8Ftrv/V9L9eBHEByMSu/zfb0lJXtFLOTvXiOVhUuQ+dVE3y7JLM+i7o8/3Avq6XwprTFacwAWwzYtZfQRh/Zwp38G39YIYK8E4mdDBtbs5s1V3Pojc134NQEucl1wEMXcKD7BZG7JKzKIBTKtOfnO3jc8vbjWPMiY5flMgihbmHPsKzBliLmZhrb8CO5boKR1vsN21n1Mh5TKpH/po=";

        private const string net35AESIv = "7J3qwSPkzX77Ifh7n9fkgg==";
        private const string net35AESKey = "qVrKNvfsCrDrTFx56u74i62L6gB2AICKYVEfmnlekRc=";


        [Test(Description = "[NetCore] Encrypt and decrypt with generated NetCore RSA key should work"),
         TestCaseSource(typeof(MyDataClass), "TestCases")]
        public void EncryptAndDecryptWithGeneratedNetCoreRSAKeyShouldWork(string value)
        {
            var keys = CryptoHandler.CreateRSAKeyPair(2560);
            var privateKey = keys.Item1;
            var publicKey = keys.Item2;

            var encoded = CryptoHandler.EncryptRSA(value, publicKey);
            var decoded = CryptoHandler.DecryptRSA(encoded, privateKey);

            encoded.Should().NotBeNullOrWhiteSpace();
            encoded.Should().NotBe(value);
            encoded.Should().NotBe(decoded);
            decoded.Should().Be(value);
        }

        [Test(Description = "[NetCore] Encrypt and decrypt with generated WinRT RSA key should work"),
         TestCaseSource(typeof(MyDataClass), "TestCases")]
        public void EncryptAndDecryptWithGeneratedWinRTRSAKeyWork(string value)
        {
            var encoded = CryptoHandler.EncryptRSA(value, winRtRSAPublicKey);
            var decoded = CryptoHandler.DecryptRSA(encoded, winRtRSAPrivateKey);

            encoded.Should().NotBeNullOrWhiteSpace();
            encoded.Should().NotBe(value);
            encoded.Should().NotBe(decoded);
            decoded.Should().Be(value);
        }

        [Test(Description = "[NetCore] Encrypt and decrypt with generated Net 3.5 RSA key should work"),
         TestCaseSource(typeof(MyDataClass), "TestCases")]
        public void EncryptAndDecryptWithGeneratedNet35RSAKeyWork(string value)
        {
            var encoded = CryptoHandler.EncryptRSA(value, net35RSAPublicKey);
            var decoded = CryptoHandler.DecryptRSA(encoded, net35RSAPrivateKey);

            encoded.Should().NotBeNullOrWhiteSpace();
            encoded.Should().NotBe(value);
            encoded.Should().NotBe(decoded);
            decoded.Should().Be(value);
        }

        [Test(Description = "[NetCore] Encrypt and decrypt with generated NetCore AES key should work"),
         TestCaseSource(typeof(MyDataClass), "TestCases")]
        public void EncryptAndDecryptWithGeneratedNetCoreAESKeyShouldWork(string value)
        {
            var keyPair = CryptoHandler.CreateAESKeyIVPair();
            var key = keyPair.Item2;
            var iv = keyPair.Item1;
            var encoded = CryptoHandler.EncryptAES(value, key, iv);
            var decoded = CryptoHandler.DecryptAES(encoded, key, iv);

            encoded.Should().NotBeNullOrWhiteSpace();
            encoded.Should().NotBe(value);
            encoded.Should().NotBe(decoded);
            decoded.Should().Be(value);
        }

        [Test(Description = "[NetCore] Encrypt and decrypt with generated WinRT AES key should work"),
         TestCaseSource(typeof(MyDataClass), "TestCases")]
        public void EncryptAndDecryptWithGeneratedWinRTAESKeyWork(string value)
        {
            var encoded = CryptoHandler.EncryptAES(value, winRtAESKey, winRtAESIv);
            var decoded = CryptoHandler.DecryptAES(encoded, winRtAESKey, winRtAESIv);

            encoded.Should().NotBeNullOrWhiteSpace();
            encoded.Should().NotBe(value);
            encoded.Should().NotBe(decoded);
            decoded.Should().Be(value);
        }

        [Test(Description = "[NetCore] Encrypt and decrypt with generated Net 3.5 AES key should work"),
         TestCaseSource(typeof(MyDataClass), "TestCases")]
        public void EncryptAndDecryptWithGeneratedNet35AESKeyWork(string value)
        {
            var encoded = CryptoHandler.EncryptAES(value, net35AESKey, net35AESIv);
            var decoded = CryptoHandler.DecryptAES(encoded, net35AESKey, net35AESIv);

            encoded.Should().NotBeNullOrWhiteSpace();
            encoded.Should().NotBe(value);
            encoded.Should().NotBe(decoded);
            decoded.Should().Be(value);
        }
    }

    public class MyDataClass
    {
        public static IEnumerable TestCases
        {
            get
            {
                yield return new TestCaseData("teste de string 1");
                yield return new TestCaseData("not empty");
                yield return new TestCaseData("one more time with @/*");
                yield return new TestCaseData("uma string em pt com josé e ramelê");
            }
        }
    }
}
