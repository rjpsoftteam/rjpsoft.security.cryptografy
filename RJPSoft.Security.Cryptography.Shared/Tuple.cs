﻿namespace RJPSoft.Security.Cryptography
{
    /// <summary>
    /// Holds two values
    /// </summary>
    /// <typeparam name="T1">The type of the 1.</typeparam>
    /// <typeparam name="T2">The type of the 2.</typeparam>
    public class Tuple<T1, T2>
    {
        /// <summary>
        /// Gets the first value.
        /// </summary>
        public T1 Item1 { get; }

        /// <summary>
        /// Gets the second value.
        /// </summary>
        public T2 Item2 { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Tuple{T1, T2}"/> class.
        /// </summary>
        /// <param name="item1">The first value.</param>
        /// <param name="item2">The second value.</param>
        public Tuple(T1 item1, T2 item2)
        {
            Item1 = item1;
            Item2 = item2;
        }

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        public override string ToString()
        {
            return $"{nameof(Item1)}: {Item1} -|- {nameof(Item2)}: {Item2}";
        }
    }

}
