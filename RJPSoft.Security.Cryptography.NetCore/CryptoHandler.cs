﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace RJPSoft.Security.Cryptography
{
    /// <summary>
    /// Handles Cryptografy
    /// </summary>
    public static class CryptoHandler
    {
        #region Public Methods

        #region RSA

        /// <summary>
        /// Encrypts using RSA.
        /// </summary>
        /// <param name="data">The data to be encrypted.</param>
        /// <param name="publicKey">The encription public key.</param>
        /// <returns>The data encrypted</returns>
        public static string EncryptRSA(string data, string publicKey)
        {
            using (var rsaProvider = new RSACryptoServiceProvider())
            {
                rsaProvider.ImportCspBlob(Convert.FromBase64String(publicKey));
                var plainBytes = Encoding.UTF8.GetBytes(data);
                var encryptedBytes = rsaProvider.Encrypt(plainBytes, false);
                return Convert.ToBase64String(encryptedBytes);
            }
        }

        /// <summary>
        /// Decrypts using RSA.
        /// </summary>
        /// <param name="data">The data to be decrypted.</param>
        /// <param name="privateKey">The private decryption key.</param>
        /// <returns>The decrypted data</returns>
        public static string DecryptRSA(string data, string privateKey)
        {
            using (var rsaProvider = new RSACryptoServiceProvider())
            {
                rsaProvider.ImportCspBlob(Convert.FromBase64String(privateKey));
                var plainBytes = rsaProvider.Decrypt(Convert.FromBase64String(data), false);
                var plainText = Encoding.UTF8.GetString(plainBytes, 0, plainBytes.Length);
                return plainText;
            }
        }

        /// <summary>
        /// Create private and public keys pair.
        /// </summary>
        /// <param name="keySize">The size of the key</param>
        /// <returns>The private and public keys</returns>
        public static Tuple<string, string> CreateRSAKeyPair(int keySize)
        {
            using (var rsaProvider = new RSACryptoServiceProvider(keySize))
            {
                var publicKey = Convert.ToBase64String(rsaProvider.ExportCspBlob(false));
                var privateKey = Convert.ToBase64String(rsaProvider.ExportCspBlob(true));
                return new Tuple<string, string>(privateKey, publicKey);
            }
        }

        #endregion RSA

        #region AES

        /// <summary>
        /// Decrypts using aes.
        /// </summary>
        /// <param name="encryptedData">The data to be encrypted.</param>
        /// <param name="key">The encryption key.</param>
        /// <param name="iv">The initialization vector.</param>
        /// <returns>The decrypted data</returns>
        public static string DecryptAES(string encryptedData, string key, string iv)
        {
            using (var aes = Aes.Create())
            {
                var _key = Convert.FromBase64String(key);
                var _iv = Convert.FromBase64String(iv);
                var encryptor = aes.CreateDecryptor(_key, _iv);
                using (var ms = new MemoryStream())
                using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                {
                    var bytes = Convert.FromBase64String(encryptedData);
                    cs.Write(bytes, 0, bytes.Length);
                    cs.FlushFinalBlock();
                    var str = Encoding.UTF8.GetString(ms.ToArray());
                    return str;
                }
            }
        }

        /// <summary>
        /// Encrypts using aes.
        /// </summary>
        /// <param name="data">The data to be encrypted.</param>
        /// <param name="key">The encryption key.</param>
        /// <param name="iv">The initialization vector.</param>
        /// <returns>The encrypted data</returns>
        public static string EncryptAES(string data, string key, string iv)
        {
            using (var aes = Aes.Create())
            {
                var _key = Convert.FromBase64String(key);
                var _iv = Convert.FromBase64String(iv);
                var encryptor = aes.CreateEncryptor(_key, _iv);
                var bytes = Encoding.UTF8.GetBytes(data);

                using (var ms = new MemoryStream())
                using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                {
                    cs.Write(bytes, 0, bytes.Length);
                    cs.FlushFinalBlock();
                    var str = Convert.ToBase64String(ms.ToArray());
                    return str;
                }
            }
        }

        public static Tuple<string, string> CreateAESKeyIVPair()
        {
            using (var aes = Aes.Create())
            {
                var iv = Convert.ToBase64String(aes.IV);
                var key = Convert.ToBase64String(aes.Key);
                return new Tuple<string, string>(iv, key);
            }
        }

        #endregion AES

        /// <summary>
        /// Hashes the value using SHA256
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The hashed value</returns>
        public static string ApplySha256(string value)
        {
            var cyphedValue = string.Empty;
            using (var sha = SHA256.Create())
            {
                var bytesAppId = sha.ComputeHash(Encoding.UTF8.GetBytes(value));
                cyphedValue = Convert.ToBase64String(bytesAppId);
            }

            return cyphedValue;
        }

        #endregion Public Methods
    }
}