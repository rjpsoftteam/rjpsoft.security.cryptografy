﻿using RJPSoft.Security.Cryptography;
using Windows.UI.Xaml.Controls;
namespace TestCripto
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void GenRSAButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var keys = CryptoHandler.CreateRSAKeyPair(2560);
            rsaPrivateKeyTextBox.Text = keys.Item1;
            rsaPublicKeyTextBox.Text = keys.Item2;
        }

        private void GenAESButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var keys = CryptoHandler.CreateAESKeyIVPair();
            aesIvTextBox.Text = keys.Item1;
            aesKeyTextBox.Text = keys.Item2;
        }

        private void Hash256Button_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var hashedValue = CryptoHandler.ApplySha256(OriginalDataTextBox.Text);
            hashedTextBox.Text = hashedValue;
        }

        private void EncryptRSAButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            encriptedTextBox.Text = CryptoHandler.EncryptRSA(OriginalDataTextBox.Text, rsaPublicKeyTextBox.Text);
        }

        private void EncryptAESButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            encriptedTextBox.Text = CryptoHandler.EncryptAES(OriginalDataTextBox.Text, aesKeyTextBox.Text, aesIvTextBox.Text);
        }

        private void DecryptRSAButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            decriptedTextBox.Text = CryptoHandler.DecryptRSA(encriptedTextBox.Text, rsaPrivateKeyTextBox.Text);
        }

        private void DecryptAES_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            decriptedTextBox.Text = CryptoHandler.DecryptAES(encriptedTextBox.Text, aesKeyTextBox.Text, aesIvTextBox.Text);
        }
    }
}
